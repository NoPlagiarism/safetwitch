#!/bin/sh

ROOT_DIR=/app
# Save old variables

# Get old vars
if [ -f .env.old ]; then
  export $(echo $(cat .env.old | sed 's/#.*//g'| xargs) | envsubst)
else
  export SAFETWITCH_BACKEND_DOMAIN_PLACEHOLDER=SAFETWITCH_BACKEND_DOMAIN_PLACEHOLDER
  export SAFETWITCH_INSTANCE_DOMAIN_PLACEHOLDER=SAFETWITCH_INSTANCE_DOMAIN_PLACEHOLDER
  export SAFETWITCH_HTTPS_PLACEHOLDER=SAFETWITCH_HTTPS_PLACEHOLDER
fi

echo $SAFETWITCH_BACKEND_DOMAIN_PLACEHOLDER

# Replace env vars in files served by NGINX
for file in $ROOT_DIR/assets/*.js $ROOT_DIR/index.html;
do
  sed -i 's|'${SAFETWITCH_BACKEND_DOMAIN_PLACEHOLDER}'|'${SAFETWITCH_BACKEND_DOMAIN}'|g' $file
  sed -i 's|'${SAFETWITCH_INSTANCE_DOMAIN_PLACEHOLDER}'|'${SAFETWITCH_INSTANCE_DOMAIN}'|g' $file
  sed -i 's|'${SAFETWITCH_HTTPS_PLACEHOLDER}'|'${SAFETWITCH_HTTPS}'|g' $file
  # Your other variables here...
done

# Save old variables
echo -e "SAFETWITCH_BACKEND_DOMAIN_PLACEHOLDER=${SAFETWITCH_BACKEND_DOMAIN}" > .env.old
echo -e "SAFETWITCH_INSTANCE_DOMAIN_PLACEHOLDER=${SAFETWITCH_INSTANCE_DOMAIN}" >> .env.old
echo -e "SAFETWITCH_HTTPS_PLACEHOLDER=${SAFETWITCH_HTTPS}" >> .env.old

# Starting NGINX
nginx -g 'daemon off;'